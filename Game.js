const textElement = document.getElementById(`text`)
const optionButtonsElement = document.getElementById(`option-buttons`)

let state = {}

function startGame() {
    state = {}
    showTextNode(1)
}

function showTextNode(textNodeIndex) {
    const textNode = textNodes.find(textNode => textNode.id === textNodeIndex)
    textElement.innerText = textNode.text
    while (optionButtonsElement.firstChild) {
        optionButtonsElement.removeChild(optionButtonsElement.firstChild)
    }

    textNode.options.forEach(option => {
        if (showOption(option)) {
            const button = document.createElement(`button`)
            button.innerText = option.text
            button.classList.add('btn')
            button.addEventListener(`click`, () => selectOption(option))
            optionButtonsElement.appendChild(button)

        }
    })
}

function showOption(option) {
    return option.requiredState == null || option.requiredState(state)
}

function selectOption(option) {
    const nextTextNodeID = option.nextText
    if (nextTextNodeID <= 0) {
        return startGame()
    }
    state = Object.assign(state, option.setState)
    showTextNode(nextTextNodeID)
}

const textNodes = [
    {
        id: 1,
        text: " You start the day as any normal day, to your left is a path to the woods and to your right is an open path and field.",
        options: [
            {
                text: "Do you go left?",
                setState: { goLeft: true },
                nextText: 2
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 3
            }
        ]
    },
    {
        id: 2,
        text: "You enjoy a trek into the woods and come across a dark opening away from the main path. You can go left and sway from the main path or you can go right and carry on the main path because it's kinda scary.",
        options: [
            {
                text: "Do you go left?",
                setState: { goleft: true },
                nextText: 4
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 5

            }
        ]
    },
    {
        id: 3,
        text: "The open field is ahead of you and you are enjoying the sunshine. In front you see what appears to be an outline of a dog do you take the left path and avoid the dog or the right path and walk toward the dog?.. I mean everyone loves dogs right?",
        options: [
            {
                text: "Do you go left?",
                setState: { goleft: true },
                nextText: 6
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 7
            }
        ]
    },
    {
        id: 4,
        text: "You sway from the main path and put on your big boy/girl pants on. You come across a cave with a strange glimmer, you can go left and head into the cave, laughing in the face of danger or you can go right and leave the cave.",
        options: [
            {
                text: "Do you go left?",
                setState: { goleft: true },
                nextText: 8
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 9
            }
        ]
    },
    {
        id: 5,
        text: "You stick to the main path and come across someone trying to steal a ladys bag(this just got interesting), you can turn left and avoid them because who knows what could happen or you can turn right to catch the thug and who knows, you may become the next avenger.",
        options: [
            {
                text: "Do you go left?",
                setState: { goleft: true },
                nextText: 10
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 11
            }
        ]
    },
    {
        id: 6,
        text: "You quickly turn the other way, I mean not all dogs are friendly right? Only now you see an old fling heading towards you(I know were reusing the whole heading towards you bit get over it). If you turn left you can avoid your old fling I mean their old flings for a reason right but if you turn right you can casually bump into them and pretend you hadnt even noticed them and then start the whole O hey  its you bla bla (you get the idea).",
        options: [
            {
                text: "Do you go left?",
                setState: { goleft: true },
                nextText: 12
            },
            {
                text: "Do you go right?",
                setState: { goRight: true },
                nextText: 13
            }
        ]
    },
    {
        id: 7,
        text: "You head towards the dog, you love dogs why wouldnt you, you little rascal... Only... its actually an escaped tiger and your gruesomely mauled to death, I mean it really isnt pretty people are screaming children are crying it's just plain awful. I suppose the moral here is you should have gone to specsavers.",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
    
            }
        ]

    },
    {
        id: 8,
        text: "You charge into that cave knowing you still have those big boy/girl pants and discover a lost treasure and become the next millionare over the coming days who needs Chris Tarrent or Jeramy Clarkson for those newer viewers out there ( how do you find him as a host I think he great). Anyhoo Great find! ",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },
    {
        id: 9,
        text: "You decide not to head into the cave, smart choice really I mean what if there was a cave in, it could be all over. You head home get your favourite meal on and whack on the 6 o'clock news. You see that your neighbour who you dont like that much has found a lost treasure in the very cave you decided not to go in. You loudly scream a various mix of profanitys at your tv who for younger audiences I shall not repeat. It's a real bummer and im betting you wish you went into that cave now.",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },
    {
        id: 10,
        text: "You chose to avoid the thug and snuck around, who knows what happened to that lady, but at least your home and safe ey......Until a knock on the door(who could it be) IT'S THE FUZZ!!! Your dna and footprints are linked to the scene and you go to jail for a crime you didnt commit and the real thug becomes a supervillian that goes on to terrorise the world. Farfetched i know but if you dont look at the logic too much it could work. Could even be a great set up for a movie.",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },
    {
        id: 11,
        text: "You turn right and without any regards for your own safety, boldy charge the individual knocking him out cold..check you out. The police are called and you are praised a local hero. Well done you, better than that other ending trust me, even if your not the next avenger. ",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },
    {
        id: 12,
        text: "You turn left and avoid your old fling, while walking home you are tragically hit by a car (yeah we kinda turned dark here)in your last moments  you can only wonder what would have happened if you turned right.",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },
    {
        id: 13,
        text: "You turn right and catch up with your fling and agree to go on a date, things go well and as cliche as it is you live happily ever after... i know i want to throw up too.",
        options: [
            {
                text: `restart and get another ending it could be better but it could be worse`,
                nextText: -1
            }
        ]
    },



]


startGame()